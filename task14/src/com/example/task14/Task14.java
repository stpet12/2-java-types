package com.example.task14;

import com.sun.jdi.Value;

public class Task14 {


    public static int reverse(int value) {
        String resultStr = String.valueOf(value);
        resultStr = new StringBuilder(resultStr).reverse().toString();
        int result = Integer.parseInt(resultStr);
        return result;
    }

    public static void main(String[] args) {
        int result = reverse(345);
        System.out.println(result);
    }


}
